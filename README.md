# Scrap Location data

It will scrap longitude and latitude from restaurant page of mentioned sites.

## How to Run

You need a selenium `webdriver` depend on browser you are using. For chrome you can download it from [here](https://chromedriver.chromium.org/downloads)

### Install all the required package

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install all requiremet.

```
pip install -r requirements.txt
```

### Run

```
python3 main.py
```

You will Prompted for enter location. Type the location in that country press Enter.
> **_Location:_**  Chinatown.


___
Collecting Restaurants data of page: `<pageNumber>`

All location Fetched Successfully.
Your files is saved in : `<fileName>`
