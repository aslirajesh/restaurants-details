import time
import json
from datetime import datetime
from bs4 import BeautifulSoup
from seleniumwire import webdriver
from seleniumwire.utils import decode
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

URL = "https://food.grab.com/sg/en"
DRIVER_PATH = "./chromedriver"


def scrap_location():
    location = input("Enter any name of place in country:  ")
    print('Scrapping Started...' + '\n' + 'Please Wait....' + '\n')
    # Define Driver Path
    driver = webdriver.Chrome(DRIVER_PATH)

    # minimize the window
    driver.minimize_window()

    # Url link for site to refer
    driver.get(URL)

    # find Search box
    try:
        search_input_box = driver.find_element(By.CLASS_NAME, 'ant-input')
    except NoSuchElementException:
        print("Make sure You are connected with VPN")
        return

    # Enter text in box
    search_input_box.send_keys(location)
    time.sleep(20)

    search_input_box.send_keys(Keys.ENTER)

    # Submit button action
    search_btn = driver.find_element(By.XPATH, '/html/body/div[1]/div['
                                               '2]/div[3]/div[2]/div/button')
    search_btn.click()
    time.sleep(10)

    list_of_all_restaurant = []

    # add data from landing page to list
    soup = BeautifulSoup(driver.page_source, "html.parser")
    initial_data_from_script = soup.find('script',
                                         {'id': '__NEXT_DATA__'}).text
    first_page = json.loads(initial_data_from_script)

    try:
        first_page_restaurants = first_page['props']['initialReduxState'][
            'pageRestaurantsV2']['entities']['restaurantList']
    except KeyError:
        print("No restaurant found at given location")
        return

    for _, restaurant_data in first_page_restaurants.items():
        single_restaurant_data = {'latitude': restaurant_data['latitude'],
                                  'longitude': restaurant_data['longitude'],
                                  'name': restaurant_data['name']}
        list_of_all_restaurant.append(single_restaurant_data)

    # click load more button to load more data until no load more button
    page = 0
    while True:
        try:
            load_more_button = driver.find_element(
                By.CLASS_NAME, 'ant-btn.ant-btn-block')
            load_more_button.click()
            time.sleep(10)
            print("Collecting Restaurants data of page:  "
                  + str(page), end="\r")
            page += 1
        except NoSuchElementException:
            break

    # Now retrieve all data from previous all request
    for request in driver.requests:
        if request.response and request.url == \
                'https://portal.grab.com/foodweb/v2/search' and \
                request.response.status_code == 200 and \
                request.method == 'POST':
            res = decode(request.response.body,
                         request.response.headers.get('Content-Encoding',
                                                      'identity'))
            res = res.decode("utf8")

            json_result = json.loads(res)['searchResult']['searchMerchants']
            for restaurant in json_result:
                single_restaurant_data = {
                    'latitude': restaurant['latlng']['latitude'],
                    'longitude': restaurant['latlng']['longitude'],
                    'name': restaurant['address']['name']}

                list_of_all_restaurant.append(single_restaurant_data)

    file_name = datetime.now().strftime("%y%d%m_%H%M%S") + ".json"

    with open(file_name, "w") as fw:
        data = {"data": list_of_all_restaurant}
        json.dump(data, fw)
        print("All location Fetched Successfully. Your files is saved in : "
              + file_name)


scrap_location()
